'use strict';

agCookie.create('example-cookie', true, 1);

var readValue = agCookie.read('example-cookie');

console.log(readValue);

agCookie.erase('example-cookie');

function markoFunkcija(args) {
    console.log(args);

    return true;
}
'use strict';

$(document).ready(function () {

	console.log('My Dudes, its Friday...');

	$('.nav-icon1').click(function () {
		console.log("its clicked my dudes...");
		$(this).toggleClass('open');
		$(".main_menu").toggleClass("expand_me");
		$(".btn_ani").toggleClass("move_me");
	});

	$('.menu_item').click(function () {
		$('.nav-icon1').toggleClass('open');
		$(".main_menu").toggleClass("expand_me");
		console.log("clicked Pt2");
	});

	$('.slider').slick({
		arrows: false,
		dots: true
	});

	$('.test_slider').slick({
		dots: true,
		nextArrow: '<i class="right icon-right-open-big"></i>',
		prevArrow: '<i class="left icon-left-open-big"></i>'
	});

	var waypoint = new Waypoint({
		element: document.getElementById('basic-waypoint'),
		handler: function handler() {
			$('.counter').each(function () {
				var $this = $(this),
				    countTo = $this.attr('data-count');

				$({ countNum: $this.text() }).animate({
					countNum: countTo
				}, {

					duration: 2000,
					easing: 'linear',
					step: function step() {
						$this.text(Math.floor(this.countNum));
					},
					complete: function complete() {
						$this.text(this.countNum);
					}
				});
			});
		},
		offset: '50%'
	});
});